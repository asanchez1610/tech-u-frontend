import { html } from 'lit-html';
import '../src/main-app.js';

export default {
  title: 'tech-u-frontend',
};

export const App = () =>
  html`
    <main-app></main-app>
  `;
