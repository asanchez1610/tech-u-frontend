import { css } from 'lit-element';
export default css`
:host([hidden]),
[hidden] {
  display: none !important;
}

.main-page {
  margin: 0;
  margin-left: 90px;
  margin-top: 90px;
  background-color: white;
  width: calc(100vw - 120px);
  padding: 20px 25px;
  min-height: calc(100vh - 115px);
}

.card {
  border-radius: 0;
}

.bg-default-head {
  background-color: #ecf0f1;
}

.breadcrumb {
  padding-left: 0;
  margin-bottom: 15px;
  padding-bottom: 5px;
}

.breadcrumb-item iron-icon {
  width: 19px;
  height: 19px;
  margin-right: 5px;
}

.toolbar-panel {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.toolbar-panel .actions-buttons {
  display: flex;
  align-items: center;
  justify-content: flex-end;
}

.content-breadcrumb {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.content-breadcrumb .content-buttons {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.content-breadcrumb .content-buttons .btn-arqueo {
  margin: 0 0 0 5px;
}

.hide {
  display: none;
}

.toolbar-page {
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
}

.toolbar-page .inputs {
  display: flex;
}

.toolbar-page .inputs input {
  margin-right: 5px;
}

.toolbar-page .inputs .btn-search {
  padding: 0 10px;
}

.toolbar-page .inputs .btn-search-large {
  display: none;
}

.toolbar-page .buttons {
  display: flex;
}

.toolbar-page .buttons button {
  margin-left: 5px;
  outline: none;
}

@media only screen and (max-width: 768px) {
  .main-page {
    margin-left: 3%;
    width: calc(97% - 12px);
  }
  .card {
    margin-bottom: 1em;
  }
  .toolbar-panel {
    display: block;
  }
  .toolbar-panel .title-panel {
    text-align: center;
    margin-bottom: 10px;
  }
  .toolbar-panel .actions-buttons {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .content-breadcrumb {
    display: block;
    width: 100%;
  }
  .content-breadcrumb .content-buttons {
    margin-bottom: 15px;
  }
  .toolbar-page {
    display: block;
  }
  .toolbar-page .inputs {
    display: flex;
  }
  .toolbar-page .inputs input {
    margin-right: 5px;
  }
  .toolbar-page .inputs .btn-search {
    padding: 0 10px;
  }
  .toolbar-page .buttons {
    display: flex;
    margin-top: 10px;
    justify-content: flex-end;
  }
  .toolbar-page .buttons button {
    margin-left: 5px;
    outline: none;
  }
}

@media only screen and (max-width: 576px) {
  .toolbar-page {
    display: block;
  }
  .toolbar-page .inputs {
    display: block;
  }
  .toolbar-page .inputs input {
    margin-right: 5px;
  }
  .toolbar-page .inputs .btn-search {
    padding: 0 10px;
    display: none;
  }
  .toolbar-page .inputs .btn-search-large {
    margin-top: 15px;
    display: block;
  }
  .toolbar-page .buttons {
    display: flex;
    margin-top: 10px;
    justify-content: space-between;
  }
  .toolbar-page .buttons button {
    margin-left: 0px;
    outline: none;
  }
}

.modal-open-wc {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1050;
  width: 100%;
  height: 100%;
  overflow: hidden;
  outline: 0;
  background-color: rgba(0, 0, 0, 0.7);
}

.btn-up {
  position: fixed;
  bottom: 10px;
  right: 25px;
  z-index: 200;
}
`;