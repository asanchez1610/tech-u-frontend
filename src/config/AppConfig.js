/*
#Example theme light
{
  ...
  defaultTheme: 'light',
  defaultThemeBootstrap: 'lux',
  urlLogo: './assets/images/logo_bbva_azul.svg',
  urlLogoWhite: './assets/images/logo_bbva_blanco.svg',
  colorTextLogo: '#000',
  ...
}

#Example theme green
{
  ...
  defaultTheme: 'green',
  defaultThemeBootstrap: 'lux',
  urlLogo: './assets/images/logo_bbva_blanco.svg',
  urlLogoWhite: './assets/images/logo_bbva_blanco.svg',
  colorTextLogo: '#fff',
  ...
}

#Example theme blue
{
  ...
  defaultTheme: 'blue',
  defaultThemeBootstrap: 'lux',
  urlLogo: './assets/images/logo_bbva_blanco.svg',
  urlLogoWhite: './assets/images/logo_bbva_blanco.svg',
  colorTextLogo: '#fff',
  ...
}

#Example theme pink
{
  ...
  defaultTheme: 'pink',
  defaultThemeBootstrap: 'lux',
  urlLogo: './assets/images/logo_bbva_blanco.svg',
  urlLogoWhite: './assets/images/logo_bbva_blanco.svg',
  colorTextLogo: '#fff',
  ...
}
*/

const cts = {
  titleApp: 'ArchBox 2.0',
  defaultPage: 'home',
  imgProfile: './assets/images/avatar-male.svg',
  defaultTheme: 'blue',
  defaultThemeBootstrap: 'lux',
  urlLogo: './assets/images/logo_bbva_blanco.svg',
  urlLogoWhite: './assets/images/logo_bbva_blanco.svg',
  colorTextLogo: '#fff',
  options: [
    {
      name: 'Inicio',
      page: 'home',
      icon: 'home'
    },
    {
      name: 'Arqueo de caja',
      page: 'arqueo-caja',
      icon: 'editor:monetization-on'
    },
    {
      name: 'Control de arqueo diario',
      page: 'control-arqueo',
      icon: 'receipt'
    },
    {
      name: 'Valores del sistema',
      page: 'gvalores',
      icon: 'device:storage',
      disabled: true
    }

  ]
};

const columnsConfig = {
  misCuadresRealizados: [
    {
      title: 'Fecha',
      dataField: 'fechaSolicitud',
      columnRender: (value, index, row) => {
        return `${window.moment(value.trim()).format('DD/MM/YYYY')}`;
      }
    },
    {
      title: 'Revisado',
      dataField: 'saldoRevisadoSoles',
      columnRender: (value, index, row) => {

        return `<div class="saldos">
                  <div><div class = "circle soles" >S/</div>${row.saldoRevisadoSoles}</div>
                  <div><div class = "circle dolares" >$</div>${row.saldoRevisadoDolares}</div>
                  <div><div class = "circle euros" >€</div>${row.saldoRevisadoDolares}</div>
                </div>`;
      }
    },
    {
      title: 'Sobrante',
      dataField: 'saldoRevisadoSoles',
      columnRender: (value, index, row) => {

        let difS = row.saldoSolesBalancin - row.saldoRevisadoSoles
        let difD = row.saldoDolaresBalancin - row.saldoRevisadoDolares
        let difE = row.saldoEurosBalancin - row.saldoRevisadoEuros

        let colorS = '#121212';
        let colorD = '#121212';
        let colorE = '#121212';
        if(difS > 0) {
          colorS = '#AD53A1';
        } else {
          difS = 0;
        }
        if(difD > 0) {
          colorS = '#AD53A1';
        } else {
          difD = 0;
        }
        if(difE > 0) {
          colorS = '#AD53A1';
        } else {
          difE = 0;
        }
        return `<div class="saldos">
        <div style ="color:${colorS}"><div class ="other">${difS}</div></div>
        <div style ="color:${colorD}" ><div class ="other">${difD}</div></div>
        <div style ="color:${colorE}" ><div class ="other">${difE}</div></div>
      </div>`;
      }
    },
    {
      title: 'Faltante',
      dataField: 'saldoRevisadoSoles',
      columnRender: (value, index, row) => {
        let difS = row.saldoSolesBalancin - row.saldoRevisadoSoles
        let difD = row.saldoDolaresBalancin - row.saldoRevisadoDolares
        let difE = row.saldoEurosBalancin - row.saldoRevisadoEuros

        let colorS = '#121212';
        let colorD = '#121212';
        let colorE = '#121212';
        if(difS < 0) {
          difS = difS * (-1);
          colorS = '#DA3851';
        } else {
          difS = 0;
        }
        if(difD < 0) {
          colorD = '#DA3851';
          difD = difD * (-1);
        } else {
          difD = 0;
        }
        if(difE < 0) {
          colorE = '#DA3851';
          difE = difE * (-1);
        } else {
          difE = 0;
        }
        return `<div class="saldos">
        <div style ="color:${colorS}"><div class ="other">${difS}</div></div>
        <div style ="color:${colorD}" ><div class ="other">${difD}</div></div>
        <div style ="color:${colorE}" ><div class ="other">${difE}</div></div>
      </div>`;
      }
    },
    {
      title: 'Estado',
      dataField: 'estado',
      columnRender: (value, index, row) => {
        return `${value ? value : ''}`;
      }
    }
  ],
  misCuadreSupervisados: [
    {
      title: 'Fecha',
      dataField: 'fechaSolicitud',
      columnRender: (value, index, row) => {
        return `${window.moment(value.trim()).format('DD/MM/YYYY')}`;
      }
    },
    {
      title: 'Revisado',
      dataField: 'saldoRevisadoSoles',
      columnRender: (value, index, row) => {

        return `<div class="saldos">
                  <div><div class = "circle soles" >S/</div>${row.saldoRevisadoSoles}</div>
                  <div><div class = "circle dolares" >$</div>${row.saldoRevisadoDolares}</div>
                  <div><div class = "circle euros" >€</div>${row.saldoRevisadoDolares}</div>
                </div>`;
      }
    },
    {
      title: 'Sobrante',
      dataField: 'saldoRevisadoSoles',
      columnRender: (value, index, row) => {

        let difS = row.saldoSolesBalancin - row.saldoRevisadoSoles
        let difD = row.saldoDolaresBalancin - row.saldoRevisadoDolares
        let difE = row.saldoEurosBalancin - row.saldoRevisadoEuros

        let colorS = '#121212';
        let colorD = '#121212';
        let colorE = '#121212';
        if(difS > 0) {
          colorS = '#AD53A1';
        } else {
          difS = 0;
        }
        if(difD > 0) {
          colorS = '#AD53A1';
        } else {
          difD = 0;
        }
        if(difE > 0) {
          colorS = '#AD53A1';
        } else {
          difE = 0;
        }
        return `<div class="saldos">
        <div style ="color:${colorS}"><div class ="other">${difS}</div></div>
        <div style ="color:${colorD}" ><div class ="other">${difD}</div></div>
        <div style ="color:${colorE}" ><div class ="other">${difE}</div></div>
      </div>`;
      }
    },
    {
      title: 'Faltante',
      dataField: 'saldoRevisadoSoles',
      columnRender: (value, index, row) => {
        let difS = row.saldoSolesBalancin - row.saldoRevisadoSoles
        let difD = row.saldoDolaresBalancin - row.saldoRevisadoDolares
        let difE = row.saldoEurosBalancin - row.saldoRevisadoEuros

        let colorS = '#121212';
        let colorD = '#121212';
        let colorE = '#121212';
        if(difS < 0) {
          difS = difS * (-1);
          colorS = '#DA3851';
        } else {
          difS = 0;
        }
        if(difD < 0) {
          colorD = '#DA3851';
          difD = difD * (-1);
        } else {
          difD = 0;
        }
        if(difE < 0) {
          colorE = '#DA3851';
          difE = difE * (-1);
        } else {
          difE = 0;
        }
        return `<div class="saldos">
        <div style ="color:${colorS}"><div class ="other">${difS}</div></div>
        <div style ="color:${colorD}" ><div class ="other">${difD}</div></div>
        <div style ="color:${colorE}" ><div class ="other">${difE}</div></div>
      </div>`;
      }
    },
    {
      title: 'Empleado',
      dataField: 'estado',
      columnRender: (value, index, row) => {
        return `${value ? value : ''}`;
      }
    }
  ]
};

const services = {
  host: 'https://back-strong-develop.com',
  endPoints: {
    auth: 'auth',
    groups: 'api/groups',
    arqueoCaja:'api/arqueo-caja',
    supervisar: 'api/arqueo-caja'
  }
}


export { cts, services, columnsConfig };