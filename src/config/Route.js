const routers = [
    { page: 'login-page', path: 'login'},
    { page: 'admin-page', path: 'admin'},
    { page: 'control-arqueo-page', path: 'control-arqueo'},
    { page: 'arqueo-caja-page', path: 'arqueo-caja'},
    { page: 'arqueo-caja-page', path: 'arqueo-caja/:id'},
    { page: 'gvalores-page', path: 'gvalores'},
    { page: 'home-page', path: 'home'}
];

export { routers };