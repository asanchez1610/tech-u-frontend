import { BaseElement } from '../../core/BaseElement.js';
import { AuthService } from './AuthService.js';

class AuthDm extends BaseElement {

  get authService() {
    return new AuthService();
  }

  async login(userName, password) {
    const { response, status } = await this.authService.auth(userName, password);
    console.log('status', status);
    if (status < 200 || status > 299) {
      this.dispatch('login-auth-error', response);
    } else {
      window.sessionStorage.setItem('registro', response.registro);
      window.sessionStorage.setItem('token', response.token);
      window.sessionStorage.setItem('userSession', JSON.stringify(response));
      this.dispatch('login-auth-success', response);
    }

  }

}
customElements.define('auth-dm', AuthDm);