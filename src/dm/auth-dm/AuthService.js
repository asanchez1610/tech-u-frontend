import { BaseService } from '../../core/BaseService.js';

export class AuthService extends BaseService {

  constructor() {
    super();
  }

  async auth(userName, password) {
    const body = JSON.stringify({ registro: userName, password: password });
    const request = await this.generateRequest({
      method: 'POST',
      path: this.endPoints.auth,
      noToken: true,
      body: body,
      headers: [
        {
          name: 'Content-Type',
          value: 'application/json'
        }
      ]
    });
    const { status } = request;
    const response = await request.json();
    return { response, status };
  }

}