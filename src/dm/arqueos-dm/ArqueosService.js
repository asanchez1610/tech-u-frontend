import { BaseService } from '../../core/BaseService.js';

export class ArqueosService extends BaseService {

  constructor() {
    super();
  }

  async findArqueos(params) {
    params = params || {};
    if(!params.fecIni) {
      params.fecIni= window.moment().subtract(7, "days").format('YYYYMMDD');
    }
    if(!params.fecFin) {
      params.fecFin = window.moment().add(1, "days").format('YYYYMMDD');
    } else {
      let date = window.moment(params.fecFin, 'YYYYMMDD').add(1, "days").format('YYYYMMDD');
      params.fecFin = date;
    }
    const request = await this.generateRequest({
      path: `${this.endPoints.arqueoCaja}?fechaInicio=${params.fecIni}&fechaFin=${params.fecFin}`
    });
    const { status } = request;
    let response;
    if(status === 204) {
      response = {};
    } else {
      response = await request.json();
    }
    return { response, status };
  }

  async saveArqueo(arqueo) {
    const request = await this.generateRequest({
      method: 'POST',
      path: this.endPoints.arqueoCaja,
      body: arqueo,
      headers: [
        {
          name: 'Content-Type',
          value: 'application/json'
        }
      ]
    });
    const { status } = request;
    const response = await request.json();
    return { response, status };
  }


  async supervisar(arqueo) {
    const request = await this.generateRequest({
      method: 'PUT',
      body: arqueo,
      path: `${this.endPoints.supervisar}/${arqueo.id}`,
      headers: [
        {
          name: 'Content-Type',
          value: 'application/json'
        }
      ]
    });
    const { status } = request;
    const response = await request.json();
    return { response, status };
  }

}