import { BaseElement } from '../../core/BaseElement.js';
import { ArqueosService } from './ArqueosService.js';

class ArqueoDm extends BaseElement {

  static get properties() {
    return {
      autoLoadRevisados: String,
      autoLoadSupervisados: String
    };
  }

  constructor() {
    super()
    this.service = new ArqueosService();
    this.autoLoadRevisados = '';
    this.autoLoadSupervisados = '';
    this.initDm();
  }

  async initDm() {
    await this.updateComplete;
    if (this.autoLoadRevisados === 'y') {
      this.findArqueos();
    }
  }

  async findArqueos(params) {
    this.dispatch('before-load-arqueos-data', {});
    const { response } = await this.service.findArqueos(params);
    this.dispatch('load-arqueos-data', response);
  }

  async saveArqueo(group) {
    this.dispatch('before-save-arqueo', {});
    const { response, status } = await this.service.saveArqueo(group);
    this.dispatch('before-save-arqueo', response);
    return { response, status };
  }

  async supervisar(arqueo) {
    this.dispatch('before-supervisar-arqueo', {});
    const { response, status} = await this.service.supervisar(arqueo);
    this.dispatch('before-supervisar-arqueo', response);
    return { response, status };
  }

}
customElements.define('arqueo-dm', ArqueoDm);