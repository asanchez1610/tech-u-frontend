import { BaseService } from '../../core/BaseService.js';

export class GValoresService extends BaseService {

  constructor() {
    super();
  }

  async findGroups() {
    const request = await this.generateRequest({
      path: this.endPoints.groups
    });
    const { status } = request;
    let response;
    if(status === 204) {
      response = {};
    } else {
      response = await request.json();
    }
    return { response, status };
  }

  async saveGroup(group) {
    const request = await this.generateRequest({
      method: 'POST',
      path: this.endPoints.groups,
      body: group,
      headers: [
        {
          name: 'Content-Type',
          value: 'application/json'
        }
      ]
    });
    const { status } = request;
    const response = await request.json();
    return { response, status };
  }

}