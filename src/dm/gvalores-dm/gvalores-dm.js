import { BaseElement } from '../../core/BaseElement.js';
import { GValoresService } from './GValoresService.js';

class GValoresDm extends BaseElement {

  static get properties() {
    return { autoLoadGroups: String };
  }

  constructor() {
    super()
    this.service = new GValoresService();
    this.autoLoadGroups = '';
    this.initDm();
  }

  async initDm() {

    await this.updateComplete;
    if (this.autoLoadGroups === 'y') {
      this.findGroups();
    }
  }

  async findGroups() {
    this.dispatch('before-load-groups-values-data', {});
    const { response } = await this.service.findGroups();
    this.dispatch('load-groups-values-data', response);
  }

  async saveGroups(group) {
    this.dispatch('before-save-group', {});
    const { response } = await this.service.saveGroup(group);
    this.dispatch('before-save-group', response);
    return response;
  }

}
customElements.define('gvalores-dm', GValoresDm);