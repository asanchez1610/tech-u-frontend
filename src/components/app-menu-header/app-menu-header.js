import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js'
import styles from './app-menu-header-style.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/communication-icons';
import '@polymer/iron-icons/editor-icons';
import '@polymer/iron-icons/social-icons';
import '@polymer/iron-icons/image-icons';
import '@polymer/iron-icons/device-icons';
import '@polymer/iron-icons/av-icons';
import '@polymer/iron-icons/maps-icons';
import '@polymer/iron-icons/hardware-icons';
class AppMenuHeader extends BaseElement {

  static get styles() {
    return [styles]
  }

  static get properties() {
    return {
      titleApp: { type: String },
      defaultPage: { type: String },
      items: { type: Array },
      theme: { type: String },
      logo: { type: String },
      colorTextLogo: { type: String },
      logoMenu: { type: String },
      imgProfile: { type: String },
      defaultIconMenuOption: { type: String },
      stateCollapsed: { type: Boolean },
      timerTooltip: { type: Number },
      user: { type: Object },
      imgFProfile: { type: String },
      imgMProfile: { type: String }
    };
  }

  constructor() {
    super();
    this.theme = 'dark';
    this.titleApp = 'BoxArch 2.0';
    this.defaultIconMenuOption = 'radio-button-checked';
    this.items = [];
    this.timerTooltip = 0;
    this.colorTextLogo = 'white';
    this.user = this.userSession;
    this.imgFProfile = '../../../assets/images/avatar-famele.svg';
    this.imgMProfile = '../../../assets/images/avatar-male.svg';
    this.readyInit();
  }

  async readyInit() {
    await this.updateComplete;
    this.removeAllOptionsActive();
    this.currentPage();
    window.addEventListener('click', (e) => {
      this.element('.wrapper-aside').classList.remove('collapsible');
      this.element('.box-info-user-profile').classList.remove('show-box-info-user');
      this.element('.tooltipMenu').style.display = 'none';
      this.stateCollapsed = false;
    });
  }

  collapsedSidebar(e) {
    e.stopPropagation();
    this.element('.box-info-user-profile').classList.remove('show-box-info-user');
    this.element('.wrapper-aside').classList.toggle('collapsible');
    if (this.element('.wrapper-aside').classList.contains('collapsible')) {
      this.stateCollapsed = true;
    } else {
      this.stateCollapsed = false;
    }
  }

  selectedOptionMenu(e, item, id) {
    e.stopPropagation();
    this.dispatch('selected-option-menu', item);
    this.removeAllOptionsActive();
    this.element(`#${id}`).classList.add('active');
    window.location = `/#${item.page}`;
    this.element('.wrapper-aside').classList.remove('collapsible');
    this.element('.box-info-user-profile').classList.remove('show-box-info-user');
    this.stateCollapsed = false;
  }

  get optionsElements() {
    return this.elementsAll('.item-option-menu') || [];
  }

  removeAllOptionsActive() {
    if (this.optionsElements) {
      this.optionsElements.forEach(item => {
        item.classList.remove('active');
      });
    }
  }

  hoverIconOption(evt, type, text) {
    if (this.stateCollapsed) {
      this.element('.tooltipMenu').style.display = 'none';
      return;
    }
    const bound = evt.target.getBoundingClientRect();
    const top = bound.top;
    clearTimeout(this.timerTooltip);
    this.timerTooltip = setTimeout(() => {
      this.element('.tooltipMenu').innerHTML = '';
      if (type === 'out') {
        this.element('.tooltipMenu').style.display = 'none';
      } else {
        this.element('.tooltipMenu').innerHTML = text;
        this.element('.tooltipMenu').style.display = 'block';
        this.element('.tooltipMenu').style.top = `${top - 7}px`;
      }
    }, 100);

  }

  goHome() {
    this.removeAllOptionsActive();
    window.location = '/#';
  }

  showInfoProfile(e) {
    e.stopPropagation();
    this.element('.box-info-user-profile').classList.toggle('show-box-info-user');
    this.element('.wrapper-aside').classList.remove('collapsible');
  }

  logOut() {
    this.dispatch('log-out-application', {});
  }

  goProfile() {
    this.dispatch('go-profile', {});
  }

  get imageUser() {
    if (this.user && this.user.urlImgProfile) {
      return this.user.urlImgProfile;
    }
    if (this.user && this.user.sexo === 'F') {
      return this.imgFProfile;
    } else {
      return this.imgMProfile;
    }
  }

  get titleLogoTpl() {
    return html` <h2 style="${this.colorTextLogo}">${this.titleApp}</h2>`;
  }

  async currentPage() {
    let path = window.location.href.toString();
    path = path.substr(path.indexOf('#'));
    path = path.replace(/#/gi, '');
    let items = this.shadowRoot.querySelectorAll('.item-option-menu');
    items.forEach((item) => {
      if (item.id === `option-menu-${path}`) {
        item.classList.add('active');
      }
    });
    if(path.length === 0) {
      this.removeAllOptionsActive();
    }
  }

  render() {
    return html`
    ${this.includeBootstrap()}
    <header class="${this.theme}">
      <section class="section-top-left">
        <div class="btn-menu-responsive">
          <iron-icon class="${this.theme === 'light' ? 'text-dark' : ''}" @click="${this.collapsedSidebar}" icon="menu">
          </iron-icon>
        </div>
        <div class="logo">
          <img @click="${this.goHome}" src="${this.logo}">
          ${this.titleLogoTpl}
        </div>

      </section>
      <section class="section-top-right">

        <div class="text-info-user">
          <span>${this.extract(this.user, 'nombres', 'N/A')} ${this.extract(this.user, 'apellidos', 'N/A')}</span>
          <span class="rol">${this.extract(this.user, 'rol.name', '--')}</span>
        </div>

        <img @click="${this.showInfoProfile}" src="${this.imageUser}">

      </section>
    </header>

    <div class="wrapper-aside">
      <aside>
        <section class="header-logo-sidebar">

          <iron-icon id="menu-icon" @click="${this.collapsedSidebar}" icon="menu"></iron-icon>

          <div class="logo-menu">
            <img @click="${this.goHome}" src="${this.logoMenu}">
            <h2>${this.titleApp}</h2>
          </div>

        </section>
        <nav>
          <ul>

            ${this.items.map((item) => html`
            <li ?hidden="${item.disabled}" id="option-menu-${item.page}" class="item-option-menu"
              @click="${(e) => this.selectedOptionMenu(e, item, `option-menu-${item.page}`)}">

              <div class="icon-content" @mouseover="${(e) => this.hoverIconOption(e, 'in', item.name)}"
                @mouseout="${(e) => this.hoverIconOption(e, 'out')}">

                <iron-icon icon="${item.icon ? item.icon : this.defaultIconMenuOption}"></iron-icon>
              </div>
              <div class="link-text-content">
                <a href="#${item.page}"></a>${item.name}</a>
              </div>
            </li>
            `)}

          </ul>
        </nav>
      </aside>
    </div>

    <div class="tooltipMenu"></div>

    <div class="box-info-user-profile">
      <img src="${this.imageUser}">
      <div class="name">${this.extract(this.user, 'nombres', 'N/A')} ${this.extract(this.user, 'apellidos', 'N/A')}</div>
      <div class="registro" @click="${this.goProfile}">${this.extract(this.user, 'registro', '--')}</div>
      <div class="other-text">
        <iron-icon icon="home"></iron-icon>
        <span>${this.extract(this.user, 'oficina.name', '--')} - ${this.extract(this.user, 'oficina.code', '--')}</span>
      </div>
      <div class="other-text">
        <iron-icon icon="mail"></iron-icon>
        <span>${this.extract(this.user, 'email', 'No Registrado')}</span>
      </div>
      <button @click="${this.logOut}" type="button" class="btn btn-sm btn-secondary btn-block btnLogOut">Cerrar
        sesión</button>
    </div>

    `
  }
}
customElements.define('app-menu-header', AppMenuHeader);