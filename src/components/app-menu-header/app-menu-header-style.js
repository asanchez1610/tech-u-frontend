import { css } from 'lit-element';
export default css`
:root {
  margin: 0;
  padding: 0;
}

header {
  width: 100%;
  height: 65px;
  box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.1);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 0.25s ease-in-out;
}

.section-top-right {
  margin-right: 15px;
  display: flex;
  align-items: center;
}

.section-top-right img {
  width: 38px;
  height: 37px;
  border-radius: 50%;
  cursor: pointer;
  background-color: rgba(228, 228, 228, 0.7);
  box-shadow: 0px 1px 1px 1px rgba(117, 117, 117, 0.8);
}

.section-top-right .text-info-user {
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  font-size: 0.82em;
  margin-right: 15px;
  text-transform: uppercase;
  line-height: 1.3em;
}

.section-top-right .text-info-user .rol {
  font-size: 0.8em;
  font-weight: bold;
}

.blue {
  background-color: #004481;
  color: #fff;
}

.blue-light {
  background-color: #1464A5;
  color: #fff;
}

.navy {
  background-color: #072146;
  color: #fff;
}

.green {
  background-color: #006c6c;
  color: #fff;
  color: #fff;
}

.light {
  background-color: #fff;
  color: #666;
}

.pink {
  background-color: #ad53a1;
  color: #fff;
}

.section-top-left {
  margin-left: 75px;
}

.section-top-left .btn-menu-responsive {
  display: none;
  width: 75px;
  height: 65px;
  background-color: transparent;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
}

.section-top-left .btn-menu-responsive iron-icon {
  height: 29px;
  width: 29px;
  margin-left: 15px;
  margin-right: 15px;
  color: white;
  cursor: pointer;
  margin-top: 19px;
  height: 27px;
  width: 27px;
}

.section-top-left .logo {
  width: 240px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  color: white;
}

.section-top-left .logo img {
  width: 80px;
  cursor: pointer;
}

.section-top-left .logo h2 {
  margin: 0;
  margin-left: 10px;
  padding: 0;
  width: calc(100% - $w_log);
  font-size: 1em;
  font-weight: normal;
}

.wrapper-aside {
  margin: 0;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 15;
  background-color: #2b2f3a;
  height: 100vh;
  width: 60px;
  overflow: hidden;
  transition: width 0.15s linear;
  border-right: 1px solid rgba(69, 65, 78, 0.95);
}

.collapsible {
  width: 300px;
}

aside .header-logo-sidebar {
  margin: 0;
  background-color: #0f1011;
  height: 65px;
  width: 300px;
  display: flex;
  align-items: center;
}

aside .header-logo-sidebar iron-icon {
  height: 29px;
  width: 29px;
  margin-left: 15px;
  margin-right: 15px;
  color: white;
  cursor: pointer;
}

aside .header-logo-sidebar .logo-menu {
  width: 240px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  color: white;
  margin-left: 18px;
}

aside .header-logo-sidebar .logo-menu img {
  width: 80px;
  cursor: pointer;
}

aside .header-logo-sidebar .logo-menu h2 {
  margin: 0;
  margin-left: 10px;
  padding: 0;
  width: calc(100% - $w_log);
  font-size: 1em;
  font-weight: normal;
}

aside nav {
  color: white;
  padding: 0;
  margin: 0;
}

aside nav ul {
  padding: 0;
  margin: 0;
  width: 300px;
  overflow: hidden;
}

aside .active {
  color: white !important;
}

aside nav ul > li {
  list-style: none;
  display: flex;
  align-items: center;
  padding: 15px 0;
  border-bottom: 1px solid rgba(12, 12, 12, 0.35);
  font-size: 0.9em;
  color: #8991a9;
  transition: all 0.25s ease-in-out;
  cursor: pointer;
}

aside nav ul > li .icon-content {
  width: 65px;
}

aside nav ul > li .icon-content iron-icon {
  margin-left: 17px;
  height: 20px;
  width: 20px;
}

aside nav ul > li .link-text-content {
  padding-left: 10px;
  width: calc(100% - $h_items);
}

aside nav ul > li a {
  color: inherit;
  text-decoration: none;
}

aside nav ul > li:hover {
  color: #fff;
}

.tooltipMenu {
  position: fixed;
  padding: 12px 15px;
  left: 65px;
  top: 0;
  background-color: #2b2f3a;
  color: white;
  z-index: 15;
  display: none;
  font-size: 0.9em;
}

.tooltipMenu::after {
  content: " ";
  position: absolute;
  top: 50%;
  right: 100%;
  /* To the left of the tooltip */
  margin-top: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent #2b2f3a transparent transparent;
}

@media only screen and (max-width: 768px) {
  .wrapper-aside {
    width: 0;
    border-right: none;
  }
  .collapsible {
    width: 300px;
  }
  .section-top-left .btn-menu-responsive {
    display: block;
  }
}

.text-dark {
  color: #000;
}

.text-white {
  color: #fff;
}

.box-info-user-profile {
  background-color: #4d4d4d;
  box-shadow: 0 0 5px #4d4d4d;
  width: 280px;
  position: fixed;
  top: 70px;
  right: -300px;
  transition: all 0.25s ease-in-out;
  color: #fff;
  padding: 15px;
  display: flex;
  align-items: center;
  flex-direction: column;
  font-size: 0.9em;
  z-index: 20;
  text-transform: uppercase;
  padding-bottom: 20px;
}

.box-info-user-profile img {
  margin-top: 10px;
  width: 55%;
  border-radius: 50%;
  background-color: rgba(117, 117, 117, 0.8);
  box-shadow: 0px 1px 1px 1px rgba(117, 117, 117, 0.8);
}

.box-info-user-profile .name {
  margin-top: 25px;
  text-align: center;
}

.box-info-user-profile .registro {
  font-size: 0.9em;
  font-weight: bold;
  color: #5bbeff;
  margin-top: 5px;
  margin-bottom: 5px;
  cursor: pointer;
}

.box-info-user-profile .registro:hover {
  text-decoration: underline;
}

.box-info-user-profile .other-text {
  margin: 0px 0 0px 0;
  font-size: 0.85em;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.8em;
  text-align: center;
}

.box-info-user-profile .other-text iron-icon {
  width: 16px;
  margin-right: 5px;
}

.box-info-user-profile .btnLogOut {
  margin-top: 10px;
}

.show-box-info-user {
  right: 10px;
}

@media only screen and (max-width: 768px) {
  .section-top-right .text-info-user {
    display: none;
  }
}
`;