import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js'
import styles from './app-login-style.js';
class AppLogin extends BaseElement {
  static get styles() {
    return [styles]
  }

  static get properties() {
    return {
      hasError: { type: Boolean },
      msgError: { type: String }
    };
  }

  constructor() {
    super();
    this.msgError = 'Error de usuario o clave.';
  }

  messageSuccessAuth(data) {
    this.element('.container-login').style.display = 'none';
    this.element('h2').innerHTML = `Bienvenid${data.sexo === 'F' ? 'a' : 'o'} <span style="text-transform: capitalize !important;" >${data.nombres.toLowerCase()}!</span>`;
    this.element('h2').classList.add('success-form');
  }

  showPwd(e) {
    console.log(this.element('#pwd').type);
    if ('password' === this.element('#pwd').type) {
      this.element('#pwd').setAttribute('type', 'text');
      e.target.setAttribute('icon', 'visibility-off');
    } else {
      this.element('#pwd').setAttribute('type', 'password');
      e.target.setAttribute('icon', 'visibility');
    }
  }

  validate() {
    const elements = this.elementsAll('.input-item');
    let count = 0;
    elements.forEach(item => {
      if (item.value.length === 0) {
        item.parentNode.classList.add('invalid');
        count++;
      } else {
        item.parentNode.classList.remove('invalid');
      }
    });
    return count === 0;
  }

  login() {
    if (this.validate()) {
      this.element('#btn-loading').setAttribute('disabled', 'disabled');
      this.element('#img-loading').classList.remove('hide');
      this.dispatch('on-send-login', {
        userName: this.element('#userName').value,
        password: this.element('#pwd').value
      })
    }
  }

  authError(error) {
    if(error && error.message) {
      this.msgError = error.message;
    } else {
      this.msgError = 'Error de usuario o clave.';
    }
    this.element('#btn-loading').removeAttribute('disabled');
    this.element('#img-loading').classList.add('hide');
    this.hasError = true;
  }

  clearSend() {
    this.element('#btn-loading').removeAttribute('disabled');
    this.element('#img-loading').classList.add('hide');
    this.hasError = false;
  }

  enterSend(e) {
    if (e.key === 'Enter' || e.keyCode === 13) {
      this.login();
    }
  }

  render() {
    return html`
      <div class="wrapper-login">

        <div class="login-form-content ">
          <h2>Bienvenido</h2>
          <div class="container-login">
            <div class="container-input">
              <input @keyup="${this.enterSend}" autocomplete="off" id="userName" class="input-item" placeholder="Usuario">
              <iron-icon icon="perm-identity"></iron-icon>
            </div>
            <div class="container-input">
              <input @keyup="${this.enterSend}" autocomplete="off" id="pwd" class="input-item" type="password"
                placeholder="Password">
              <iron-icon @click="${this.showPwd}" style="cursor:pointer;" icon="visibility"></iron-icon>
            </div>
            <div class="container-input">
              <button id="btn-loading" @click="${this.login}">Acceder</button>
              <img id="img-loading" class="loadin-send hide" src="./assets/images/spinner-white.svg">
            </div>
            <div class="container-msg-error" ?hidden="${!this.hasError}" >
                    ${this.msgError}
            </div>
          </div>
        </div>


      </div>
    `
  }
}
customElements.define('app-login', AppLogin);