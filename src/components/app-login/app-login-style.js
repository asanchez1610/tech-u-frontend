import { css } from 'lit-element';
export default css`
:host([hidden]), [hidden] {
  display: none !important;
}

.wrapper-login {
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  background: url("../../../assets/images/blue-background.svg");
  z-index: 1500;
  background-size: auto;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-family: "Montserrat", sans-serif;
}

.wrapper-login .login-form-content {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
}

.wrapper-login .login-form-content h2 {
  text-align: center;
  font-weight: lighter;
  color: #fff;
  font-size: 2.7em;
  margin-bottom: 30px;
  transition: all 0.38s linear;
  margin-top: -120px;
}

.wrapper-login .login-form-content .success-form {
  margin-top: 0px;
}

.wrapper-login .login-form-content .container-login {
  display: flex;
  justify-content: center;
  min-width: 320px;
  flex-direction: column;
  transition: all 0.2s ease-in-out;
}

.wrapper-login .login-form-content .container-login input {
  font-family: "Montserrat", sans-serif;
  width: calc(100% - 38px);
  padding: 10px 33px 10px 5px;
  outline: none;
  background: transparent;
  border: none;
  border-bottom: 1px solid #e1e1e1;
  color: #fff;
  font-size: 1.2em;
  font-weight: lighter;
  margin-bottom: 30px;
}

.wrapper-login .login-form-content .container-login input::placeholder {
  color: #fff;
}

.wrapper-login .login-form-content .container-login button {
  background: transparent;
  border: none;
  outline: none;
  padding: 10px 2px;
  font-size: 1.2em;
  font-family: "Montserrat", sans-serif;
  font-weight: lighter;
  color: #fff;
  border: 1px solid #fff;
  border-radius: 2px;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
}

.wrapper-login .login-form-content .container-login button:hover {
  color: #d4edfc;
}

.wrapper-login .login-form-content .container-login .container-msg-error {
  margin-top: 25px;
  text-align: center;
  color: #F35E61;
  font-weight: normal;
  font-size: 1.2em;
}

.container-input {
  position: relative;
}

.container-input iron-icon {
  position: absolute;
  right: 3px;
  top: 11px;
  color: #fff;
  height: 25px;
  width: 25px;
}

.container-input button {
  width: 100%;
}

.container-input .loadin-send {
  position: absolute;
  top: 7px;
  right: 7px;
  width: 30px;
}

@media only screen and (max-height: 375px) {
  .wrapper-login .login-form-content h2 {
    margin-top: -80px;
  }
}

.hide {
  display: none;
}

.invalid input {
  border-bottom: 1px solid #F35E61 !important;
}

.invalid input::placeholder {
  color: #F35E61 !important;
  font-weight: normal;
}

.invalid iron-icon {
  color: #F35E61 !important;
}
`;