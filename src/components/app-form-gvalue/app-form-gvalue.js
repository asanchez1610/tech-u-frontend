import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js'
import styles from './app-form-gvalue-style.js';
import { cts } from "../../config/AppConfig.js";

class AppFormGValue extends BaseElement {
  static get styles() {
    return [styles]
  }

  static get properties() {
    return {};
  }

  cancel() {
    this.dispatch('cancel-form-gvalues', {});
  }

  validate() {
    const requeridos = this.elementsAll('.requerido');
    let errors = 0;
    requeridos.forEach(element => {
      if (this.isEmpty(element.value)) {
        errors++;
        element.classList.add('invalid')
      } else {
        element.classList.remove('invalid')
      }
    });
    return errors === 0;
  }

  getData() {
    let data = {};
    this.elementsAll('.input-item').forEach(item => {
      data[item.name] = item.value;
    });
    return data;
  }

  sendData() {
    if (this.validate()) {
      this.dispatch('on-data-form-gvalues', this.getData());
      this.clearInputs();
    }
  }

  clearInputs() {
    this.elementsAll('.input-item').forEach(item => {
      item.value = '';
    });
  }

  render() {
    return html`
    ${this.includeBootstrap('flatly')}
    <div class="content-form ">
      <div class="form">
        <h3>Crear grupo</h3>
        <hr>

        <label class="control-label">Código</label>
        <input autocomplete="off" name="code" class="requerido form-control form-control-sm input-item">

        <label class="control-label">Nombre</label>
        <input autocomplete="off" name="name" class="requerido form-control form-control-sm input-item">

        <label>Descripcion</label>
        <textarea name="description" class="form-control input-item" rows="2"></textarea>

        <div class="actions-buttons">
          <button type="button" class="btn btn-secondary" @click="${this.cancel}">Cancelar</button>
          <button type="button" class="btn btn-primary" @click="${this.sendData}">Grabar</button>
        </div>

      </div>
    </div>
    `
  }
}
customElements.define('app-form-gvalue', AppFormGValue);