import { html } from "lit-element";
import { BaseElement } from "../../core/BaseElement.js";
import styles from './app-form-arqueo-style.js';
import { cts } from "../../config/AppConfig.js";

class AppFormArqueo extends BaseElement {
  static get styles() {
    return [styles]
  }

  static get properties() {
    return {
      showMsg: {
        type: Boolean
      }
    };
  }

  calculateValues(e, type) {
    let v1;
    let v2;
    let dif;
    switch (type) {
      case 's':
        v1 = this.element('#bS0').value || 0;
        v2 = this.element('#rS1').value || 0;
        dif = v2 - v1;
        if (dif > 0) {
          this.element('#xS0').value = dif;
          this.element('#yS1').value = 0;
        } else if (dif < 0) {
          this.element('#yS1').value = dif * (-1);
          this.element('#xS0').value = 0;
        } else {
          this.element('#xS0').value = 0;
          this.element('#yS1').value = 0;
        }
        break;

      case 'd':
        v1 = this.element('#bD0').value || 0;
        v2 = this.element('#rD1').value || 0;
        dif = v2 - v1;
        if (dif > 0) {
          this.element('#xD0').value = dif;
          this.element('#yD1').value = 0;
        } else if (dif < 0) {
          this.element('#yD1').value = dif * (-1);
          this.element('#xD0').value = 0;
        } else {
          this.element('#xD0').value = 0;
          this.element('#yD1').value = 0;
        }
        break;

      case 'e':
        v1 = this.element('#bE0').value || 0;
        v2 = this.element('#rE1').value || 0;
        dif = v2 - v1;
        if (dif > 0) {
          this.element('#xE0').value = dif;
          this.element('#yE1').value = 0;
        } else if (dif < 0) {
          this.element('#yE1').value = dif * (-1);
          this.element('#xE0').value = 0;
        } else {
          this.element('#xE0').value = 0;
          this.element('#yE1').value = 0;
        }
        break;

      default:
        break;
    }

  }

  constructor() {
    super()
    this.showMsg = false;
  }

  validate() {
    const requeridos = this.elementsAll('.requerido');
    requeridos.forEach(element => {
      element.classList.remove('invalid');
      element.value = '0';
    });
  }

  validate() {
    const requeridos = this.elementsAll('.requerido');
    let errors = 0;
    requeridos.forEach(element => {
      if (this.isEmpty(element.value)) {
        errors++;
        element.classList.add('invalid')
      } else {
        element.classList.remove('invalid')
      }
    });
    return errors === 0;
  }

  getDataForm() {
    let data = {};
    this.elementsAll('.requerido').forEach(item => {
      data[item.name] = item.value;
    });
    return data;
  }

  registrarArqueo() {
    if (this.validate()) {
      let data = this.getDataForm();
      this.dispatch('on-register-data-arqueo', data)
    }
  }



  modeConsult({ response, status }) {
    console.log(response)
    if (!(status < 200 && status > 299)) {
      this.showMsg = true;
      this.element('.alert-danger').classList.add('hide');
      this.elementsAll('.requerido').forEach(item => {
        item.setAttribute('disabled', 'disabled');
      });
      this.element('#btn-registrar').setAttribute('disabled', 'disabled');
    } else {
      this.element('#msgError').innerHTML = response.message;
      this.element('.alert-success').classList.add('hide');
      this.element('.alert-danger').classList.remove('hide');
    }
  }

  render() {
    return html`
      ${this.includeBootstrap(cts.defaultThemeBootstrap)}

      <div class="container container-form-arqueo">

        <div class="row">

          <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">

            <h5>Saldo Balancín</h5>

            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle soles">S/</div>
                </span>
              </div>
              <input min="0" name="saldoSolesBalancin" value="0" tabindex="1" id="bS0"
                @input="${(e) => this.calculateValues(e, 's', 'xS0')}" type="number" class="form-control requerido">
            </div>


            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle dolares">$</div>
                </span>
              </div>
              <input name="saldoDolaresBalancin" min="0" value="0" tabindex="3" id="bD0"
                @input="${(e) => this.calculateValues(e, 'd', 'xD0')}" type="number" class="form-control requerido">
            </div>


            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle euros">€</div>
                </span>
              </div>
              <input name="saldoEurosBalancin" min="0" value="0" tabindex="5" id="bE0"
                @input="${(e) => this.calculateValues(e, 'e', 'xE0')}" type="number" class="form-control requerido">

            </div>

          </div>

          <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">

            <h5>Saldo Revisado</h5>

            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle soles">S/</div>
                </span>
              </div>
              <input name="saldoRevisadoSoles" min="0" value="0" tabindex="2" id="rS1"
                @input="${(e) => this.calculateValues(e, 's', 'yS1')}" type="number" class="form-control requerido">
            </div>


            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle dolares">$</div>
                </span>
              </div>
              <input name="saldoRevisadoDolares" min="0" value="0" tabindex="4" id="rD1"
                @input="${(e) => this.calculateValues(e, 'd', 'yD1')}" type="number" class="form-control requerido">
            </div>


            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle euros">€</div>
                </span>
              </div>
              <input name="saldoRevisadoEuros" min="0" value="0" tabindex="6" id="rE1"
                @input="${(e) => this.calculateValues(e, 'e', 'yE1')}" type="number" class="form-control requerido">

            </div>

          </div>

          <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 sobrante">
            <h5>Saldo Sobrante</h5>

            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle soles">S/</div>
                </span>
              </div>
              <input id="xS0" disabled value="0" class="form-control">
            </div>


            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle dolares">$</div>
                </span>
              </div>
              <input id="xD0" disabled value="0" class="form-control">
            </div>


            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle euros">€</div>
                </span>
              </div>
              <input id="xE0" disabled value="0" class="form-control">

            </div>

          </div>

          <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 faltante">

            <h5>Saldo Faltante</h5>

            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle soles">S/</div>
                </span>
              </div>
              <input id="yS1" disabled value="0" class="form-control">
            </div>


            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle dolares">$</div>
                </span>
              </div>
              <input id="yD1" disabled value="0" class="form-control">
            </div>


            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <div class="circle euros">€</div>
                </span>
              </div>
              <input id="yE1" disabled value="0" class="form-control">

            </div>
          </div>

        </div>

        <section class="bottom-bar">

          <div class="caja">

            <input name="nroCaja" type="number" min="1" tabindex="8" class="form-control requerido"
              placeholder="Número de caja">

          </div>
          <div class="buttons">
            <button id="btn-registrar" tabindex="9" type="button" class="btn btn-success"
              @click="${this.registrarArqueo}">Realizar
              Arqueo</button>
          </div>

        </section>

        <div class="alert alert-dismissible alert-success${!this.showMsg ? ' hide' : ''}">
          <button @click="${() => this.element('.alert-success').classList.add('hide')}" type="button" class="close"
            data-dismiss="alert">&times;</button>
          Se ha registrado el arqueo de caja de forma correcta, podrás ver los cambios en la página de <strong><a
              href="/#">inicio</a></strong>
        </div>

        <div class="alert alert-dismissible alert-danger hide">
          <button @click="${() => this.element('.alert-danger').classList.add('hide')}" type="button" class="close"
            data-dismiss="alert">&times;</button>
          <span id="msgError"></span>
        </div>

      </div>
      `
  }
}
customElements.define('app-form-arqueo', AppFormArqueo);