import { css } from 'lit-element';
export default css`
.container-form-arqueo {
  margin-top: 20px;
}

.container-form-arqueo .circle {
  border-radius: 50%;
  font-size: 0.9em;
  margin-right: 7px;
  width: 26px;
  height: 26px;
  text-align: center;
}

.container-form-arqueo .soles {
  background-color: #ddd7f7;
  color: #121212;
  padding: 4px 0px 3px 2px;
  font-weight: bold;
}

.container-form-arqueo .dolares {
  background-color: #88ca9a;
  color: #fff;
  padding: 4px 0px 3px 1px;
  font-weight: bold;
  font-size: 0.9em;
}

.container-form-arqueo .euros {
  background-color: #b79e5e;
  color: #fff;
  padding: 4px 0px 4px 0px;
  font-weight: bold;
  font-size: 0.9em;
}

.container-form-arqueo .input-group-text {
  padding: 0 5px 0 10px;
}

.container-form-arqueo .input-group {
  margin-bottom: 15px;
}

.container-form-arqueo .faltante input {
  color: #da3851;
}

.container-form-arqueo .sobrante input {
  color: #AD53A1;
}

.container-form-arqueo h5 {
  text-align: center;
  margin-bottom: 15px;
  font-size: 0.85em;
}

.container-form-arqueo .bottom-bar {
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 20px;
}

.container-form-arqueo .bottom-bar .caja input {
  border: 1px solid #e1e1e1;
}

.alert-success {
  margin-top: 15px;
}

.alert-danger {
  margin-top: 15px;
}

@media only screen and (max-width: 576px) {
  .bottom-bar {
    display: block !important;
    width: 100%;
    margin-top: 5px !important;
  }
  .bottom-bar .caja {
    width: 100%;
    margin-bottom: 10px;
  }
  .bottom-bar .caja input {
    border: 1px solid #e1e1e1;
  }
  .bottom-bar .buttons {
    width: 100%;
  }
  .bottom-bar .buttons button {
    width: 100%;
  }
}

.invalid {
  border-color: #dc3545 !important;
  padding-right: calc(1.5em + .75rem);
  background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
  background-repeat: no-repeat;
  background-position: right calc(.375em + .1875rem) center;
  background-size: calc(.75em + .375rem) calc(.75em + .375rem);
}

.hide {
  display: none !important;
}
`;