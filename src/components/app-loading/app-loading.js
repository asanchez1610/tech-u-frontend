import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js'
import styles from './app-loading-style.js';

class AppLoading extends BaseElement {
  static get styles() {
    return [styles]
  }

  static get properties() {
    return {};
  }

  render() {
    return html`
    <div class = "wrapper-loading-content" >
    <div class="loader"></div>
    </div>
    `
  }
}
customElements.define('app-loading', AppLoading);