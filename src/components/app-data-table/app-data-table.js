import { html } from "lit-element";
import { BaseElement } from "../../core/BaseElement.js";
import styles from "./app-data-table-style.js";
import "../custom-ui-components/CustomPaginationTable.js";
import "carbon-web-components/es/components/pagination/page-sizes-select.js";
import "carbon-web-components/es/components/pagination/pages-select.js";
import  { loadingSpinner } from '../custom-ui-components/loading.js';

class AppDataTable extends BaseElement {
  static get styles() {
    return [styles];
  }

  static get properties() {
    return {
      theme: { type: String },
      pageSize: { type: Number },
      currentPage: { type: Number },
      start: { type: Number },
      items: { type: Array },
      initialItems: { type: Array },
      columnsConfig: { type: Array },
      displayItems: { type: Array },
      minHeight: { type: String },
      bgColor: { type: String },
      enablePagination: { type: Boolean },
      paginationTop: { type: Boolean },
      paginationBottom: { type: Boolean }
    };
  }

  showLoading(value) {
    if(value) {
      this.element('.content-loading').classList.add('content-loading-show');
    } else {
      this.element('.content-loading').classList.remove('content-loading-show');
    }
  }

  constructor() {
    super();
    this.theme = 'materia';
    this.pageSize = 10;
    this.start = 0;
    this.currentPage = 1;
    this.minHeight='305px';
    this.bgColor = '#fff';
    this.enablePagination = false;
    this.paginationBottom = true;
    this.paginationTop = false;
    this.InitDataTable();
  }

  async InitDataTable() {
    await this.updateComplete;
    this.setDataDisplayItems(this.start);
    this.initialItems = [...this.items];
  }

  async setDataDisplayItems(start) {
    this.displayItems = (this.items || []).filter((item, index) => {
        return index >= start && index < (start + this.pageSize);
    });
    await this.requestUpdate();
  }

  get totalData() {
    return (this.items || []).length;
  }

  onChangedCurrent({ detail }) {
    this.setDataDisplayItems(detail.start);
  }

  onChangedPageSizesSelect({ detail }) {
    this.setDataDisplayItems(detail.start);
  }

  selectedRowData(target, item, index) {
    if (this.element(`#row-${index}`).classList.contains('table-active')) {
      this.element(`#row-${index}`).classList.remove('table-active');
      this.dispatch('un-selected-row-data-table', item);
    } else {
      this.elementsAll('.row-item').forEach(rowItem => rowItem.classList.remove('table-active'));
      this.element(`#row-${index}`).classList.add('table-active');
      this.dispatch('selected-row-data-table', item);
    }
  }

  async setItems(items) {
    this.items = [];
    this.items = items;
    this.start = 0;
    await this.setDataDisplayItems(0)
    await this.requestUpdate();
  }

  render() {
    return html`
     ${this.includeBootstrap(this.theme)}

     <div class="table-responsive">

     <div style= "position:relative;min-height:${this.minHeight}; background-color:${this.bgColor};">
     <div class="content-loading" >${loadingSpinner}</div>

     <div class="bg-pagination${(this.displayItems || []).length === 0 ? ' hide' : ''}" ?hidden="${!this.paginationTop}">

     <custom-pagination-table
      ?hidden="${!this.enablePagination}"
      page-size="${this.pageSize}"
      start="${this.start}"
      total="${this.totalData}"
      .formatStatusWithDeterminateTotal = "${({ start, end, count }) =>
        this.totalData ? `${start}–${end} de ${count} item${count <= 1 ? "" : "s"}` : ''}"
      @bx-pagination-changed-current="${this.onChangedCurrent}"
      @bx-page-sizes-select-changed="${this.onChangedPageSizesSelect}"
      >

      ${!this.totalData? undefined
          : html`
              <bx-pages-select
                .formatSupplementalText="${({ count }) => `de ${count} pág.`}"
              ></bx-pages-select>
            `
      }
      </custom-pagination-table>

     </div>

     <table class="table table-hover">
              <thead class="thead-dark">
                <tr>
                ${(this.columnsConfig || []).map(header => html`
                    <th>${header.title}</th>
                `)}
                </tr>
              </thead>
              <tbody>
                ${((this.enablePagination ? this.displayItems : this.items)  || []).map(
                  (item, index) =>
                    html`
                      <tr data-item = '${JSON.stringify(item)}'
                          class = "row-item"
                          id = "row-${index}"
                          @click = ${row => { this.selectedRowData(row, item, index); }}>

                        ${(this.columnsConfig || []).map((header, indexTd) => html`
                          <td>
                            ${header.columnRender ?
                              this.adaptStringHtml(header.columnRender(item[header.dataField], index, item))
                              :
                              item[header.dataField]}
                          </td>
                        `)}
                      </tr>
                    `
                )}
              </tbody>
    </table>

    <div class="data-empty${(this.displayItems || []).length > 0 ? ' hide' : ''}" >Sin datos para mostrar</div>
    </div>

    <div class="bg-pagination${(this.displayItems || []).length === 0 ? ' hide' : ''}" ?hidden="${!this.paginationBottom}">
    <custom-pagination-table
      ?hidden="${!this.enablePagination}"
      page-size="${this.pageSize}"
      start="${this.start}"
      total="${this.totalData}"
      .formatStatusWithDeterminateTotal = "${({ start, end, count }) =>
        this.totalData ? `${start}–${end} de ${count} item${count <= 1 ? "" : "s"}` : ''}"
      @bx-pagination-changed-current="${this.onChangedCurrent}"
      @bx-page-sizes-select-changed="${this.onChangedPageSizesSelect}"
      >

      ${!this.totalData? undefined
          : html`
              <bx-pages-select
                .formatSupplementalText="${({ count }) => `de ${count} pág.`}"
              ></bx-pages-select>
            `
      }
      </custom-pagination-table>
    </div>


  </div>


    `;
  }
}
customElements.define("app-data-table", AppDataTable);
