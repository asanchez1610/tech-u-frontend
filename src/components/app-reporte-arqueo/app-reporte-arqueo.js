import { html } from "lit-element";
import { BaseElement } from "../../core/BaseElement.js";
import styles from './app-reporte-arqueo-style.js';
import { cts } from "../../config/AppConfig.js";

class AppReporteArqueo extends BaseElement {
    static get styles() {
        return [styles]
    }

    static get properties() {
        return {
            items: { type: Array },
            open: { type: Boolean }
        };
    }

    constructor() {
        super();
        this.getDataMock();
    }

    async getDataMock() {
        let tmp = await fetch("./assets/mocks/lits-pruebas.json").then(response => response.json())
        this.items = tmp.misArqueos;
        console.log(this.items);
    }


    confirmAsig() {
        this.shadowRoot.querySelector('#exampleModal').style.display = 'block';
        this.shadowRoot.querySelector('#exampleModal').classList.add('show');
        this.shadowRoot.querySelector('#exampleModal').classList.add('modal-open-wc');
    }

    async closeModal() {
        this.shadowRoot.querySelector('#exampleModal').style.display = 'none';
        this.shadowRoot.querySelector('#exampleModal').classList.remove('show');
        this.shadowRoot.querySelector('#exampleModal').classList.remove('modal-open-wc');
    }

    render() {
        return html`
    ${this.includeBootstrap(cts.defaultThemeBootstrap)}

    <div class="container-button">
        <button @click="${this.confirmAsig}" type="button" class="btn btn-info btn-sm" data-toggle="modal"
            data-target="#exampleModal">
            Asignar Supervisores
        </button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="${this.closeModal}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Esta seguro de confirmar la asignación de supervisores?
                </div>
                <div class="modal-footer">
                    <button type="button" @click="${this.closeModal}" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-success">Si</button>
                </div>
            </div>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table table-sm table-hover">
            <thead class="thead-dark">
                <tr>
                    <th>Fecha y Hora</th>
                    <th>Creador Arqueo</th>
                    <th>Caja</th>
                    <th>Saldo Revisado</th>
                    <th>Saldo Sobrante</th>
                    <th>Saldo Faltante</th>
                    <th>Supervisor Arqueo</th>
                </tr>
            </thead>
            <tbody>

                ${(this.items || []).map(item => html`
                <tr>
                    <td >${window.moment(item.fechaSolicitud).format('DD/MM/YYYY HH:mm')}</td>
                    <td> ${item.solicitante.registro} ${item.solicitante.apellidos} ${item.solicitante.nombres}</td>
                    <td>${item.nroCaja}</td>
                    <td>
                        <div class="saldos">
                            <div><div class = "circle soles" >S/</div>${item.saldoRevisadoSoles}</div>
                            <div><div class = "circle dolares" >$</div>${item.saldoRevisadoDolares}</div>
                            <div><div class = "circle euros" >€</div>${item.saldoRevisadoDolares}</div>
                        </div>
                     </td>
                    <td>

                    <div class="saldos">
                        <div ><div class ="other">${item.sobranteSoles}</div></div>
                        <div><div class ="other">${item.sobranteDolares}</div></div>
                        <div><div class ="other">${item.sobranteEuros}</div></div>
                    </div>
                    </td>
                    <td>

                    <div class="saldos">
                        <div ><div class ="other">${item.faltanteSoles}</div></div>
                        <div><div class ="other">${item.faltanteDolares}</div></div>
                        <div><div class ="other">${item.faltanteEuros}</div></div>
                    </div>

                    </td>
                    <td> ${item.supervisor.registro} ${item.supervisor.apellidos} ${item.supervisor.nombres}
                    </td>
                </tr>

                `)}

            </tbody>
        </table>

    </div>

    `;
    }
}
customElements.define('app-reporte-arqueo', AppReporteArqueo);