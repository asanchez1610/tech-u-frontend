import { html } from 'lit-element';
import { BaseElement } from '../../core/BaseElement.js'
import styles from './app-simple-crud-style.js';
import { cts } from "../../config/AppConfig.js";
import "../../components/app-data-table/app-data-table.js";

class AppSimpleCrud extends BaseElement {
  static get styles() {
    return [ styles ]
  }

  static get properties() {
    return {
      data: Array,
      columnsConfig: Array,
      pageSize: Number,
      titlePanel: String,
      actionsTemplate: Object,
      theme: String,
      themeTable: String,
      enablePagination: Boolean,
      paginationTop: Boolean,
      paginationBottom: Boolean
     };
  }

  constructor() {
    super();
    this.pageSize = 10;
    this.data = [];
    this.columnsConfig = [];
    this.titlePanel = '';
    this.theme = cts.defaultThemeBootstrap;
    this.paginationTop = false;
    this.paginationBottom = false
    this.themeTable = 'materia';
  }

  suggestedData(e) {
    let tmp = this.data.filter(item => {
        return item.code.toLowerCase().indexOf(e.target.value) >= 0 ||
               item.name.toLowerCase().indexOf(e.target.value) >= 0
    });
    this.element('app-data-table').setItems(tmp);

  }

  async setData(data) {
    //this.data = data;
    await this.element('app-data-table').setItems(data);
  }

  onAddItem() {
    this.element('.layout-form').classList.add('layout-form-show');
    this,this.element('#txt-search').setAttribute('disabled', 'disabled');
  }

  cancel() {
    this.element('.layout-form').classList.remove('layout-form-show');
    this,this.element('#txt-search').removeAttribute('disabled');
  }

  loading(value) {
    this.element('app-data-table').showLoading(value);
  }

  render() {
    return html`
${this.includeBootstrap(cts.defaultThemeBootstrap)}
    <div class="card bg-default-head">
              <div class="card-header">
                <div class="toolbar-panel">
                  <div class="title-panel">${this.titlePanel}</div>
                  <div class="actions-buttons">
                  ${this.actionsTemplate
                      ? html`
                          ${this.actionsTemplate}
                        `
                      : html`
                           <button
                              @click="${this.onAddItem}"
                              class="btn btn-link btn-sm btn-info text-white">
                            Crear
                          </button>
                          <div class="input-group input-group-sm">
                            <input id ="txt-search" class="form-control form-control-sm" @input="${this.suggestedData}" >
                            <div class="input-group-append">
                              <div class="input-group-text constent-icon-search">
                                <iron-icon icon="search" class="icon-search"></iron-icon>
                              </div>
                            </div>
                          </div>
                        `
                    }

                  </div>
                </div>
              </div>
              <div class="card-body content-card-body">
                <app-data-table
                      .paginationTop="${this.paginationTop}"
                      .paginationBottom="${this.paginationBottom}"
                      .enablePagination="${this.enablePagination}"
                       theme = "${this.themeTable}"
                       pageSize="${this.pageSize}"
                      .columnsConfig="${this.columnsConfig}">
                </app-data-table>

                <div class="layout-form" >

                  <slot></slot>

                </div>
              </div>
            </div>
    `
  }
}
customElements.define('app-simple-crud', AppSimpleCrud);