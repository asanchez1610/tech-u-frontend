import { css } from "lit-element";
import BXPagination from "carbon-web-components/es/components/pagination/pagination";

class CustomPaginationTable extends BXPagination {
  static styles = css`
    ${BXPagination.styles}
    :host {
      outline: none;
      display: flex;
      align-items: center;
      justify-content: space-between;
      border-top: 1px solid #e1e1e1;
    }
    .bx--pagination__left {
      padding: 0;
    }

    @media (max-width: 600px) {
      :host {
        display: block;
        margin: auto;
        text-align: center;
      }
      .bx--pagination__left {
        align-items: center;
        display: flex;
        justify-content: center;
      }
      .bx--pagination__right{
        align-items: center;
        display: flex;
        justify-content: center;
      }
      
    }
  `;

  static get properties() {
    return {};
  }
}
customElements.define("custom-pagination-table", CustomPaginationTable);
