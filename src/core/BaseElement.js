import { LitElement, html } from 'lit-element';

export class BaseElement extends LitElement {

  static get properties() {
    return { service: Object };
  }

  constructor() {
    super();
  }

  extract(data, keys, value) {
    let ret;
    if (!this.isEmpty(data) && !this.isEmpty(keys)) {
      let split = keys.split('.');
      ret = data[split.shift()];
      while (ret && split.length) {
        ret = ret[split.shift()];
      }
    }
    return this.isEmpty(ret) && value !== null ? value : ret;
  }

  get userSession() {
    const userSession = window.sessionStorage.getItem('userSession');
    return JSON.parse(userSession);
  }

  isEmpty(evaluate) {
    switch (typeof (evaluate)) {
      case 'object':
        return evaluate === null || Object.keys(evaluate).length === 0;
      case 'string':
        return evaluate === '';
      case 'undefined':
        return true;
      default:
        return false;
    }
  }

  isNotEmpty(evaluate) {
    return !this.isEmpty(evaluate);
  }

  dispatch(name, detail) {
    const val = typeof detail === 'undefined' ? null : detail;
    this.dispatchEvent(new CustomEvent(name, {
      composed: true,
      bubbles: true,
      detail: val
    }));
  }

  adaptStringHtml(stringHtml) {
    var t = document.createElement('template');
    t.innerHTML = stringHtml;
    return t.content.cloneNode(true);
  }

  applyObjectParamsInUrl(path, params) {
    let urlParams = Object.keys(params).map(function (k) {
      return encodeURIComponent(k) + '=' + encodeURIComponent(params[k])
    }).join('&');
    return `${path}?${urlParams}`;
  }

  element(selector) {
    return this.shadowRoot.querySelector(selector);
  }

  elementsAll(selector) {
    return this.shadowRoot.querySelectorAll(selector);
  }

  getJsonFromQueryParams(path) {
    let query = path;
    let result = {};
    query.split("&").forEach(function (part) {
      let item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  }

  includeBootstrap(bottswach) {
    let linkBootstrap = html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
      integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    `;
    if (bottswach) {
      if (bottswach === 'yeti') {
        return html`
        ${linkBootstrap}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/yeti/bootstrap.min.css"
          integrity="sha384-mLBxp+1RMvmQmXOjBzRjqqr0dP9VHU2tb3FK6VB0fJN/AOu7/y+CAeYeWJZ4b3ii" crossorigin="anonymous">
        `;
      } else if (bottswach === 'materia') {
        return html`
        ${linkBootstrap}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/materia/bootstrap.min.css"
          integrity="sha384-B4morbeopVCSpzeC1c4nyV0d0cqvlSAfyXVfrPJa25im5p+yEN/YmhlgQP/OyMZD" crossorigin="anonymous">
        `;
      } else if (bottswach === 'pulse') {
        return html`
      ${linkBootstrap}
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/pulse/bootstrap.min.css"
        integrity="sha384-L7+YG8QLqGvxQGffJ6utDKFwmGwtLcCjtwvonVZR/Ba2VzhpMwBz51GaXnUsuYbj" crossorigin="anonymous">
        `;
      } else if (bottswach === 'lux') {
        return html`
        ${linkBootstrap}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/lux/bootstrap.min.css"
          integrity="sha384-9+PGKSqjRdkeAU7Eu4nkJU8RFaH8ace8HGXnkiKMP9I9Te0GJ4/km3L1Z8tXigpG" crossorigin="anonymous">
        `;
      } else if (bottswach === 'minty') {
        return html`
        ${linkBootstrap}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/minty/bootstrap.min.css"
          integrity="sha384-H4X+4tKc7b8s4GoMrylmy2ssQYpDHoqzPa9aKXbDwPoPUA3Ra8PA5dGzijN+ePnH" crossorigin="anonymous">
        `;
      } else if (bottswach === 'flatly') {
        return html`
        ${linkBootstrap}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/flatly/bootstrap.min.css"
          integrity="sha384-qF/QmIAj5ZaYFAeQcrQ6bfVMAh4zZlrGwTPY7T/M+iTTLJqJBJjwwnsE5Y0mV7QK" crossorigin="anonymous">
        `;
      } else {
        return html`${linkBootstrap}`;
      }
    } else {
      return html`${linkBootstrap}`;
    }

  }

}