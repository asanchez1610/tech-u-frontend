import { BaseElement } from './BaseElement';
import { html } from 'lit-element';
import { routers } from '../config/Route';
import { cts } from '../config/AppConfig';
import Navigo from 'navigo';

export class BaseApp extends BaseElement {

  static get properties() {
    return {
      route: { type: Object }
    };
  }

  constructor() {
    super();
    this.applyRoute();
  }

  async applyRoute() {
    await this.updateComplete;
    this.route = new Navigo("/", true, "#");
    this.route.notFound(() => {
      if(!window.sessionStorage.getItem('registro')) {
        window.location = '/#login';
      }
      let path = window.location.href.toString();
      path = path.substr(path.indexOf('#'));
      if (path === '/' || path === '#') {
        this.route = html`${this.adaptStringHtml(`<${cts.defaultPage}-page></${cts.defaultPage}-page>`)}`
      } else {
        this.route = html`<div style="margin: 100px;" >Pagina no encontrada</div>`
      }
    });
    routers.forEach((item) => {
      this.route.on(item.path, async () => {
        this.route =  html`${this.adaptStringHtml(`<${item.page}></${item.page}>`)}`;
      });
    });

    this.route.resolve();
    if(!window.sessionStorage.getItem('registro')) {
      window.location = '#login';
    }
  }


}