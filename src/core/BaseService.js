import { services } from '../config/AppConfig.js';

export class BaseService {

  constructor() {
    this.host = services.host;
    this.endPoints = services.endPoints;
    const userSession = window.sessionStorage.getItem('userSession');
    this.userSession = JSON.parse(userSession);
  }

  async generateRequest({ method = 'GET', path, body, headers, noToken = false, host }) {
    let sendHeaders = new Headers();
    if(!noToken) {
      sendHeaders.append("Authorization", this.userSession.token);
    }
    if (headers) {
      headers.forEach(header => {
        sendHeaders.append(header.name, header.value);
      });
    }
    const requestOptions = {
      method: method,
      headers: sendHeaders
    };
    if (body) {
      if (typeof body === 'object') {
        requestOptions.body = JSON.stringify(body);
      } else {
        requestOptions.body = body;
      }
    }
    return await fetch(`${host ? host : this.host}/${path}`, requestOptions);
  }

}