import { BaseElement } from './BaseElement';
export class BasePage extends BaseElement {

  constructor() {
    super();
    this.initializePage();
  }

  async initializePage() {
    this.maskLoading(true);
    setTimeout(() => {
      this.maskLoading(false);
    }, 700);
    this.validateSession();
    await this.updateComplete;
    if (this.onInitPage && typeof this.onInitPage === 'function') {
      this.onInitPage();
    }
  }

  validateSession() {
    if (!window.sessionStorage.getItem('registro')) {
      window.location = '/#login';
    }
  }

  maskLoading(show) {
    const event = new CustomEvent('change-loading-page', {
      detail: show,
      bubbles: true,
      composed: true
    });
    window.dispatchEvent(event);
  }

  breadcrumbTpl(pageName, tplButtons) {
    return this.adaptStringHtml(`
    <div class="content-breadcrumb">
        <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <iron-icon icon="home"></iron-icon> <a href="#">Home</a>
              </li>
              <li class="breadcrumb-item active">${pageName}</li>
        </ol>
        <div class="content-breadcrumb" >
          ${tplButtons ? tplButtons : ''}
        </div>
     </div>
    `);
  }

}