import { html, css } from 'lit-element';
import './components/app-menu-header/app-menu-header.js'
import { cts } from './config/AppConfig.js';
import './pages/admin-page/admin-page.js';
import './pages/home-page/home-page.js';
import './pages/control-arqueo-page/control-arqueo-page.js';
import './pages/arqueo-caja-page/arqueo-caja-page.js';
import './pages/login-page/login-page.js';
import './pages/gvalores-page/gvalores-page.js';
import { BaseApp } from './core/BaseApp.js';
import stylesMain from './styles/main-page-style.js';
import './components/app-loading/app-loading.js';
export class MainApp extends BaseApp {

  static get properties() {
    return {
      theme: { type: String} ,
      logo: { type: String},
      colorTextLogo: { type: String},
      showLoadingPage: { type: Boolean }
    };
  }

  static get styles() {
    return [ stylesMain ];
  }

  constructor() {
    super();
    this.theme = cts.defaultTheme;
    this.logo = cts.urlLogo;
    this.colorTextLogo = cts.colorTextLogo;
    this.initAppMain();
  }

  async initAppMain() {
    await this.updateComplete;

    window.addEventListener('change-theme-app', ({ detail }) => {
      if (typeof detail === 'string') {
        this.theme = detail;
      } else {
        this.theme = detail.theme;
        this.logo = detail.logo;
        this.colorTextLogo = detail.colorTextLogo;
      }
    });
    window.addEventListener('change-loading-page',({detail})=> {
      this.showLoadingPage = !detail;
    });
    window.addEventListener('on-change-user-session', ({detail})=> {
      console.log('on-change-user-session', detail);
      this.element('app-menu-header').user = detail;
    })
  }

  logOut() {
    window.sessionStorage.clear();
    window.location = '/#login';
  }

  render() {
    return html`
    <app-loading ?hidden="${this.showLoadingPage}"></app-loading>
    <app-menu-header
                    theme = "${this.theme}"
                    logo = "${this.logo}"
                    logoMenu = "${cts.urlLogoWhite}"
                    imgProfile="${cts.imgProfile}"
                    .items="${cts.options}"
                    colorTextLogo="${this.colorTextLogo}"
                    titleApp="${cts.titleApp}"
                    @log-out-application = "${this.logOut}"
                    ></app-menu-header>
    <main>
      ${this.route}
    </main>

    `;
  }
}
