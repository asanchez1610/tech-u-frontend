import { html } from 'lit-element';

const templateBootstrapComponents = html`
  <h2>Alerts</h2>
  <hr>
  <div class="alert alert-primary" role="alert">
    A simple primary alert—check it out!
  </div>

  <div class="alert alert-dismissible alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Well done!</strong> You successfully read <a class="alert-link">this important alert message</a>.
  </div>

  <div class="alert alert-dismissible alert-warning">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4 class="alert-heading">Warning!</h4>
    <p class="mb-0">Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent
      commodo cursus magna, <a class="alert-link">vel scelerisque nisl consectetur et</a>.</p>
  </div>

  <div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Oh snap!</strong> <a class="alert-link">Change a few things up</a> and try submitting again.
  </div>

  <div class="alert alert-dismissible alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Heads up!</strong> This <a class="alert-link">alert needs your attention</a>, but it's not super
    important.
  </div>

  <br>
  <hr>
  <h2>Buttons</h2>
  <hr>
  <button type="button" class="btn btn-primary">Primary</button>
  <button type="button" class="btn btn-secondary">Secondary</button>
  <button type="button" class="btn btn-success">Success</button>
  <button type="button" class="btn btn-info">Info</button>
  <button type="button" class="btn btn-warning">Warning</button>
  <button type="button" class="btn btn-danger">Danger</button>
  <button type="button" class="btn btn-link">Link</button>

  <br><br>
  <hr>
  <h2>Table</h2>
  <hr>
  <div class="table-responsive">
    <table class="table table-hover">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th2>
          <th scope="col">First</th2>
          <th scope="col">Last</th2>
          <th scope="col">Handle</th2>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th2>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th2>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th2>
          <td>Larry</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </table>


  </div>
  <hr>
  <h2>breadcrumbs</h2>
  <hr>
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">Home</li>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a>Home</a></li>
    <li class="breadcrumb-item active">Library</li>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a>Home</a></li>
    <li class="breadcrumb-item"><a>Library</a></li>
    <li class="breadcrumb-item active">Data</li>
  </ol>

  <hr>
  <h2>Card</h2>
  <hr>

  <div class="card mb-3">
    <h3 class="card-header">Card header</h3>
    <div class="card-body">
      <h5 class="card-title">Special title treatment</h5>
      <h6 class="card-subtitle text-muted">Support card subtitle</h6>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" class="d-block user-select-none" width="100%" height="200"
      aria-label="Placeholder: Image cap" focusable="false" role="img" preserveAspectRatio="xMidYMid slice"
      viewBox="0 0 318 180" style="font-size:1.125rem;text-anchor:middle">
      <rect width="100%" height="100%" fill="#868e96"></rect>
      <text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
    </svg>
    <div class="card-body">
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
        content.</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Cras justo odio</li>
      <li class="list-group-item">Dapibus ac facilisis in</li>
      <li class="list-group-item">Vestibulum at eros</li>
    </ul>
    <div class="card-body">
      <a class="card-link">Card link</a>
      <a class="card-link">Another link</a>
    </div>
    <div class="card-footer text-muted">
      2 days ago
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Card title</h4>
      <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
        content.</p>
      <a class="card-link">Card link</a>
      <a class="card-link">Another link</a>
    </div>
  </div>

  <hr>
  <h2>Foms</h2>
  <hr>
  <form>
    <fieldset>
      <legend>Legend</legend>
      <div class="form-group row">
        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
          <input type="text" readonly="" class="form-control-plaintext" id="staticEmail" value="email@example.com">
        </div>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
          placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="exampleSelect1">Example select</label>
        <select class="form-control" id="exampleSelect1">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </select>
      </div>
      <div class="form-group">
        <label for="exampleSelect2">Example multiple select</label>
        <select multiple="" class="form-control" id="exampleSelect2">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </select>
      </div>
      <div class="form-group">
        <label for="exampleTextarea">Example textarea</label>
        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
      </div>

      <fieldset class="form-group">
        <legend>Radio buttons</legend>
        <div class="form-check">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1"
              checked="">
            Option one is this and that—be sure to include why it's great
          </label>
        </div>
        <div class="form-check">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
            Option two can be something else and selecting it will deselect option one
          </label>
        </div>
        <div class="form-check disabled">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option3"
              disabled="">
            Option three is disabled
          </label>
        </div>
      </fieldset>
      <fieldset class="form-group">
        <legend>Checkboxes</legend>
        <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" value="" checked="">
            Option one is this and that—be sure to include why it's great
          </label>
        </div>
        <div class="form-check disabled">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" value="" disabled="">
            Option two is disabled
          </label>
        </div>
      </fieldset>
      <fieldset class="form-group">
        <legend>Sliders</legend>
        <label for="customRange1">Example range</label>
        <input type="range" class="custom-range" id="customRange1">
      </fieldset>
    </fieldset>
  </form>

  <div class="form-group">
    <fieldset disabled="">
      <label class="control-label" for="disabledInput">Disabled input</label>
      <input class="form-control" id="disabledInput" type="text" placeholder="Disabled input here..." disabled="">
    </fieldset>
  </div>

  <div class="form-group">
    <fieldset>
      <label class="control-label" for="readOnlyInput">Readonly input</label>
      <input class="form-control" id="readOnlyInput" type="text" placeholder="Readonly input here..." readonly="">
    </fieldset>
  </div>

  <div class="form-group has-success">
    <label class="form-control-label" for="inputValid">Valid input</label>
    <input type="text" value="correct value" class="form-control is-valid" id="inputValid">
    <div class="valid-feedback">Success! You've done it.</div>
  </div>

  <div class="form-group has-danger">
    <label class="form-control-label" for="inputInvalid">Invalid input</label>
    <input type="text" value="wrong value" class="form-control is-invalid" id="inputInvalid">
    <div class="invalid-feedback">Sorry, that username's taken. Try another?</div>
  </div>

  <div class="form-group">
    <label class="col-form-label col-form-label-lg" for="inputLarge">Large input</label>
    <input class="form-control form-control-lg" type="text" placeholder=".form-control-lg" id="inputLarge">
  </div>

  <div class="form-group">
    <label class="col-form-label" for="inputDefault">Default input</label>
    <input type="text" class="form-control" placeholder="Default input" id="inputDefault">
  </div>

  <div class="form-group">
    <label class="col-form-label col-form-label-sm" for="inputSmall">Small input</label>
    <input class="form-control form-control-sm" type="text" placeholder=".form-control-sm" id="inputSmall">
  </div>

  <div class="form-group">
    <label class="control-label">Input addons</label>
    <div class="form-group">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">$</span>
        </div>
        <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text">.00</span>
        </div>
      </div>
    </div>
  </div>



`;

export { templateBootstrapComponents };