import { html, css } from 'lit-element';
import { BasePage } from '../../core/BasePage';
import stylesMain from '../../styles/main-page-style.js';
import { cts } from '../../config/AppConfig.js';
import '../../components/app-form-arqueo/app-form-arqueo.js';
import '../../dm/arqueos-dm/arqueo-dm.js';
class ArqueoCajaPage extends BasePage {

  static get styles() {
    return [
      stylesMain
    ]
  }

  static get properties() {
    return {

    };
  }

  constructor() {
    super();
  }

  onInitPage() {
    let path = window.location.href.toString();
    path = path.substr(path.lastIndexOf('/'));
    path = path.replace(/\//gi, '');
    if(!this.isEmpty(path)) {
      console.log('')
    }
  }

  async registrarArqueo({ detail }) {
    console.log('registrarArqueo', detail);
    this.maskLoading(true);

    let { response, status } = await this.element('arqueo-dm').saveArqueo(detail);
    this.maskLoading(false);
    this.element('app-form-arqueo').modeConsult({ response, status });

  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
    ${this.includeBootstrap(cts.defaultThemeBootstrap)}
    <div class="main-page">
      ${this.breadcrumbTpl('Arqueo de caja')}
      <app-form-arqueo @on-register-data-arqueo="${this.registrarArqueo}"></app-form-arqueo>
    </div>
    <arqueo-dm></arqueo-dm>
    `;
  }
}
customElements.define('arqueo-caja-page', ArqueoCajaPage);