import { html } from "lit-element";
import { BasePage } from "../../core/BasePage";
import stylesMain from "../../styles/main-page-style.js";
import '../../components/app-simple-crud/app-simple-crud';
import { cts } from "../../config/AppConfig.js";
import '../../dm/gvalores-dm/gvalores-dm.js';
import '../../components/app-form-gvalue/app-form-gvalue.js';

class GValoresPage extends BasePage {
  static get styles() {
    return [stylesMain]
  }

  static get properties() {
    return {
      dataGroup: Array,
      columnsConfigGroup: Array
    };
  }

  generateMocks() {
    this.dataGroup = [];

    this.columnsConfigGroup = [
      {
        title: 'Código'
      },
      {
        title: 'Nombre'
      }
    ]

  }

  constructor() {
    super();
    this.generateMocks();
  }

  loadGroups({ detail }) {
    this.element('#crud-groups').loading(false);
    this.element('#crud-groups').setData(detail);
  }

  beforeLoadGroups() {
    this.element('#crud-groups').loading(true);
  }

  cancelForm(type) {
    if (type === 'group') {
      this.element('#crud-groups').cancel();
    } else {
      this.element('#crud-values').cancel();
    }
  }

  reloadGroups() {
    this.element('gvalores-dm').findGroups();
  }

  async sendRegisterGroup({ detail }) {
    console.log('sendRegisterGroup', detail);
    this.maskLoading(true);
    await this.element('gvalores-dm').saveGroups(detail);
    this.element('#crud-groups').cancel();
    this.maskLoading(false);
    this.reloadGroups();
  }

  render() {
    return html`
      ${this.includeBootstrap(cts.defaultThemeBootstrap)}
      <div class="main-page">

        ${this.breadcrumbTpl('Valores del sistema')}

        <div class="row">
          <div class="col-12 col-sm-12	col-md-6 col-lg-6 col-xl-6">
            <app-simple-crud titlePanel = "Grupo de valores" id="crud-groups" .data="${this.dataGroup}" pageSize="6"
              .columnsConfig="${this.columnsConfigGroup}">

              <app-form-gvalue @on-data-form-gvalues="${this.sendRegisterGroup}"
                @cancel-form-gvalues="${() => this.cancelForm('group')}"></app-form-gvalue>

            </app-simple-crud>
          </div>

          <div class="col-12 col-sm-12	col-md-6 col-lg-6 col-xl-6">
            <app-simple-crud titlePanel = "Valores del sistema" id="crud-values" .data="${this.dataGroup}" pageSize="6"
              .columnsConfig="${this.columnsConfigGroup}">

              <app-form-gvalue @on-data-form-gvalues="${this.sendRegisterGroup}"
                @cancel-form-gvalues="${() => this.cancelForm('values')}"></app-form-gvalue>

            </app-simple-crud>
          </div>
        </div>
      </div>

      <gvalores-dm autoLoadGroups='y' @before-load-groups-values-data="${this.beforeLoadGroups}"
        @load-groups-values-data="${this.loadGroups}"></gvalores-dm>
    `;
  }
}
customElements.define("gvalores-page", GValoresPage);
