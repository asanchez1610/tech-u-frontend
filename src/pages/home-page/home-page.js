import { html, css } from 'lit-element';
import { BasePage } from '../../core/BasePage';
import stylesMain from '../../styles/main-page-style.js';
import { cts, columnsConfig } from '../../config/AppConfig.js';
import '../../dm/arqueos-dm/arqueo-dm.js';

class HomePage extends BasePage {
  static get styles() {
    return [ stylesMain ]
  }

  static get properties() {
    return {
      dataRealizados: Array,
      dataSupervisados: Array,
      puedoSupervisar: { type: Boolean },
      dataSueprvision: { type: Object }
    };
  }

  constructor() {
    super();
    this.dataRealizados = [];
    this.dataSupervisados = [];
    this.puedoSupervisar = false;
    window.addEventListener('scroll', (e) => {
      if(window.scrollY > 100) {
        this.element('.btn-up').classList.remove('hide');
      } else {
        this.element('.btn-up').classList.add('hide');
      }
    });
    this.initPage()
  }

  async initPage() {
    await this.updateComplete;
    let today = moment().format('YYYY-MM-DD');
    let beforeToday = moment().subtract(7, "days").format('YYYY-MM-DD');
    this.element('#fecIni').value = beforeToday;
    this.element('#fecFin').value = today;
  }

  loadLists({ detail }) {
    this.element('#list-realizados').loading(false);
    this.element('#list-realizados').setData(detail.misArqueos);
    this.element('#list-supervisados').loading(false);
    this.element('#list-supervisados').setData(detail.misSupervisiones);
    this.dataSueprvision = detail.arqueoASupervisar;
    this.puedoSupervisar = detail.arqueoASupervisar ? true : false;
  }

  beforeLoadLists() {
    this.element('#list-realizados').loading(true);
    this.element('#list-supervisados').loading(true);
  }

  buscar() {
    let fecIni = this.element('#fecIni').value;
    let fecFin = this.element('#fecFin').value;
    if (fecIni) {
      fecIni = fecIni.replace(/-/gi, '');
    }
    if (fecFin) {
      fecFin = fecFin.replace(/-/gi, '');
    }
    console.log(fecIni);
    console.log(fecFin);
    this.element('arqueo-dm').findArqueos({ fecIni, fecFin });
  }

  async supervisar() {
    if(this.dataSueprvision) {
      this.maskLoading(true);
      console.log(this.dataSueprvision)
      await this.element('arqueo-dm').supervisar(this.dataSueprvision);
      this.buscar();
      this.maskLoading(false)
    }
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
        ${this.includeBootstrap(cts.defaultThemeBootstrap)}
        <div class="main-page">

        ${this.breadcrumbTpl('Mis Arqueos')}

        <div class = "toolbar-page" >
          <section class="inputs">
            <input id="fecIni" type="date" name="fecha" class="form-control form-control-sm">
            <input id="fecFin" type="date" name="fecha" class="form-control form-control-sm">
            <button class="btn btn-secondary btn-sm btn-search" @click="${this.buscar}" ><iron-icon icon="search" ></iron-icon></button>
            <button class="btn btn-primary btn-sm btn-block btn-search-large" @click="${this.buscar}" >Buscar</button>
          </section>
          <section class="buttons">
            <button @click="${this.supervisar}" class="btn btn-info btn-sm ${this.puedoSupervisar ? '' : 'hide'}" >Supervisar Arqueo</button>
            <button @click="${() => window.location = '#arqueo-caja'}" class="btn btn-warning btn-sm btn-arqueo">Arqueo de caja</button>
          </section>
        </div>

          <div class="row">
            <div class="col-12 col-sm-12	col-md-6 col-lg-6 col-xl-6">
              <app-simple-crud titlePanel="Mis cuadros realizados" id="list-realizados" themeTable="yeti"
                .paginationTop="${true}" .enablePagination="${true}" .data="${this.dataRealizados}" pageSize="5"
                .columnsConfig="${columnsConfig.misCuadresRealizados}" .actionsTemplate="${html``}">
              </app-simple-crud>
            </div>

            <div class="col-12 col-sm-12	col-md-6 col-lg-6 col-xl-6">
              <app-simple-crud titlePanel="Mis cuadros supervisados" id="list-supervisados" themeTable="yeti"
                .paginationTop="${true}" .enablePagination="${true}" .data="${this.dataRealizados}" pageSize="5"
                .columnsConfig="${columnsConfig.misCuadreSupervisados}" .actionsTemplate="${html``}">
              </app-simple-crud>
            </div>
          </div>
        </div>
        </div>
        <arqueo-dm
          autoLoadRevisados="y"
          @before-load-arqueos-data="${this.beforeLoadLists}"
          @load-arqueos-data="${this.loadLists}"></arqueo-dm>

        <button class="btn btn-danger btn-up hide" @click="${() => window.scrollTo(0, 0) }" ><iron-icon icon="arrow-upward" ></iron-icon></button>
    `;
  }
}
customElements.define('home-page', HomePage);