import { html, css } from 'lit-element';
import { BasePage } from '../../core/BasePage';
import stylesMain from '../../styles/main-page-style.js';
import { cts } from '../../config/AppConfig.js';
import '../../components/app-reporte-arqueo/app-reporte-arqueo.js';

class ControlArqueoPage extends BasePage {

  static get styles() {
    return [
      stylesMain
    ]
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
    window.addEventListener('scroll', (e) => {
      if(window.scrollY > 100) {
        this.element('.btn-up').classList.remove('hide');
      } else {
        this.element('.btn-up').classList.add('hide');
      }
    });
  }

  render() {
    return html`
        ${this.includeBootstrap(cts.defaultThemeBootstrap)}
        <div class="main-page">
        ${this.breadcrumbTpl('Reporte de control de arqueo')}
        <app-reporte-arqueo></app-reporte-arqueo>
        </div>
        <button class="btn btn-danger btn-up hide" @click="${() => window.scrollTo(0, 0) }" ><iron-icon icon="arrow-upward" ></iron-icon></button>
    `;
  }
}
customElements.define('control-arqueo-page', ControlArqueoPage);