import { html, css } from 'lit-element';
import { BasePage } from '../../core/BasePage';
import stylesMain from '../../styles/main-page-style.js';
import '../../components/app-login/app-login.js';
import '../../dm/auth-dm/auth-dm.js';

class LoginPage extends BasePage {

  static get styles() {
    return [
      stylesMain
    ]
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
    if(this.userSession && this.userSession.registro) {
      window.location = '/#';
    }
  }

  async sendLogin(user) {
    const { userName, password } = user
    await this.element('auth-dm').login(userName, password);
  }

  onSuccessAuth(data) {
    this.element('app-login').clearSend();
    this.element('app-login').messageSuccessAuth(data);
    setTimeout(() => {
      window.location = '#';
    }, 2000);
    this.dispatch('on-change-user-session', data);
  }

  authError(error) {
    this.element('app-login').authError(error);
  }

  render() {
    return html`
        <app-login
          @on-send-login="${({ detail }) => this.sendLogin(detail)}"></app-login>

        <auth-dm
          @login-auth-error="${({ detail }) => this.authError(detail)}"
          @login-auth-success="${({detail}) => this.onSuccessAuth(detail)}" ></auth-dm>
    `;
  }
}
customElements.define('login-page', LoginPage);