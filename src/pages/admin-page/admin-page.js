import { html, css } from 'lit-element';
import { BasePage } from '../../core/BasePage';
import stylesMain from '../../styles/main-page-style.js';
import { cts } from '../../config/AppConfig.js';
class AdminPage extends BasePage {

  static get styles() {
    return [
      stylesMain
    ]
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
    ${this.includeBootstrap(cts.defaultThemeBootstrap)}
    <div class="main-page">
      ${this.breadcrumbTpl('Administración del sistema')}
    </div>
    `;
  }
}
customElements.define('admin-page', AdminPage);